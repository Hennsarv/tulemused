﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; // selle rea lisasime, et saaks failist lugeda

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\resultaadid.txt";
            // filename sees on viide failile, mis me panime projekti kausta
            // add existing items käsuga

            string[] loetudRead = File.ReadAllLines(filename);
            // see käsk loeb failist read stringimassiivi

            // proovime kaht erinevat kollektsiooni
            List<string> nimekiri = new List<string>();
            Dictionary<string, int> koond = new Dictionary<string, int>();
            // List<string> on muidu nagu massiiv string[], 
            // aga sinna saab asju juurde lisada
            // Dictionary<string,int> on nagu massiiv, mis koosneb paarilistest 
            // võti (string) ja väärtus (int)
            // seda saab laiendada (lisada uusi ridu) ja kasutada nagu massiivi, 
            // aga siin on indeksiks string

            foreach (string loetudRida in loetudRead)
            {
                //                Console.WriteLine(loetudRida);

                // järgnevalt tükeldame loetud rea tühiku ' ' kohalt
                string nimi = loetudRida.Split(' ')[0];
                int punktid = int.Parse(loetudRida.Split(' ')[1]);
                // esimene tükk [0] on nimi
                // teine tükk [1] on puntisumma
                // selle teise teisendame int.Parsega int-iks

                // kontrolimiseks prindime
                Console.WriteLine($"{nimi} sai {punktid} punkti");

                // leitud nime pistame nimekirja
                // aga ainult siis, kui teda seal veel ei ole
                if (!nimekiri.Contains(nimi)) nimekiri.Add(nimi);

                // leitud nime jaoks teeme koonttabelis (dictionaris)
                // uue rea nime jaoks, alguses punktisumma 0
                if (!koond.Keys.Contains(nimi)) koond.Add(nimi, 0);

                // punktid loetud reast liidame selle nimega punktisummale
                koond[nimi] += punktid;

            }
            Console.WriteLine();
            foreach (string nimi in nimekiri)
                Console.WriteLine(nimi);
            Console.WriteLine();
            foreach (string nimi in koond.Keys)
                Console.WriteLine($"{nimi} sai kokku {koond[nimi]} punkti");

            // kuidas leida dictionarist koond kõige suurema punktisummaga
            string parimNimi = ""; //teeme muutuja parima nimega
                                   // alguse meil seda nime ei ole
            int parimSumma = 0;    // teeme muutuja parima punktisumma jaoks
                                   // alguses on punktisumma 0
                                   // NB! me eeldame, et kõik punktid on >0 (st negatiivselid punkte ei ole)

            // nüüd käime koondi läbi ja võrdleme
            // kui leiame kellegi, kellel on rohkem punkte,
            // jätame ta nime ja punktisumma meelde

            foreach (string nimi in koond.Keys)
                if (koond[nimi] > parimSumma)
                {
                    parimSumma = koond[nimi];
                    parimNimi = nimi;
                }
            // lõpuks trükime selle välja
            Console.WriteLine($"parim oli {parimNimi}, kes sai {parimSumma} punkti");



            // massiiv int[] string[]
            // List<int> List<String>
            // Dictionary<>

            // teeme lõppu veel ühe näite:
            Dictionary<string, List<int>> koond2 
                = new Dictionary<string, List<int>>();
            // See on dictionary, milles võtmeks (indeksiks) on string (nimi)
            // ja sisuks on List<int> - punktide loetelu
            
            // teeme pea samasuguse tsükli üle failist loetud ridade
            foreach(string loetudRida in loetudRead)
            {
                string nimi = loetudRida.Split(' ')[0];
                int punktid = int.Parse(loetudRida.Split(' ')[1]);

                // vaatame, kas leitud nimi on meil nimekirjas (koond2)
                if (!koond2.Keys.Contains(nimi))
                    koond2.Add(nimi, new List<int>());
                // kui ei ole, siis lisame - nime ja tühja listi

                koond2[nimi].Add(punktid);
                // seejärel paneme iga nime juures olevasse listi tema punktid kirja
            }

            Console.WriteLine();

            // trükime selle superdictionary välja
            foreach (string nimi in koond2.Keys)
            {
                Console.Write($"{nimi} - ");
                // kuna selle nime taga on list (kollektsioon),
                // siis käime selle teise tsükliga läbi
                foreach(int p in koond2[nimi])
                    Console.Write($"{p} ");
                Console.Write($" kokku: {koond2[nimi].Sum()} ");

                // Seda rida ma ei seleta veel teile lahti
                // kes tahab, proovib ise aru saada
                Console.Write($" parimad: {koond2[nimi].OrderByDescending(x => x).Take(3).Sum()} " );
                Console.WriteLine();
            }






        }
    }
}
